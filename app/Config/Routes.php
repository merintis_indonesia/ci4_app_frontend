<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
// $routes->get('/mis', 'Home::mis');
$routes->get('/daftar', 'Akun::index');
$routes->get('/signup', 'Home::signup');
$routes->get('/signin', 'Home::signin');
$routes->get('/lupapass', 'Home::lupapass');
$routes->get('/program', 'Home::program');
$routes->add('/program/detail/ongoing/(:num)', 'Home::ongoingdetail/$1');

$routes->get('/miadmin/homeadmin', 'Miadmin::homeadmin');
$routes->get('/miadmin/loginadmin', 'Miadmin::index');
$routes->get('/miadmin/datamis', 'Miadmin::datamis');
$routes->get('/miadmin/datafinalis', 'Miadmin::datafinalis');
$routes->get('/miadmin/dataprogram', 'Miadmin::dataprogram');
$routes->get('/miadmin/logoutadmin', 'Miadmin::logoutadmin');
$routes->get('/miadmin/exportexcel', 'Miadmin::exportexcel');
$routes->get('/miadmin/xlsfinal', 'Miadmin::exportxlsfinal');
$routes->get('/miadmin/formprogram', 'Miadmin::formprogram');
$routes->add('/miadmin/ubahprogram/(:num)', 'Miadmin::ubahprogram/$1');
$routes->add('/miadmin/hapusprogram/(:num)', 'Miadmin::hapusprogram/$1');
$routes->get('/miadmin/inputprogram', 'Miadmin::inputprogram');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
