<?= $this->extend('template_merintis/merintis.php'); ?>

<?= $this->section('head-title'); ?>
<title>Program | Merintis Indonesia</title>
<?= $this->endSection(); ?>

<?= $this->section('meta-description'); ?>
<meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
<?= $this->endSection(); ?>

<?= $this->section('addCSS'); ?>
<link rel="stylesheet" href="<?= base_url('assets/css/program.css'); ?>">
<?= $this->endSection() ?>

<?php
  $isLogin = false;
  $idAkun = '';
  $token = '';
  if(isset($_SESSION['is_login'])) {
    $isLogin = $_SESSION['is_login'];
    $idAkun = $_SESSION['id_akun'];
    $token = $_SESSION['token'];
  } else if(isset($_COOKIE['is_login'])) {
    $isLogin = $_COOKIE['is_login'];
    $idAkun = $_COOKIE['id_akun'];
    $token = $_COOKIE['token'];
  } 
?>


<?= $this->section('header'); ?>
<header>
    <nav class="navbar navbar-expand-sm navbar-dark ">
        <div class="mx-auto">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMis" aria-controls="navbarMis" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarMis">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/#beranda'); ?>">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/#tentang'); ?>">Tentang</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#program">Program</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/#team'); ?>">Team</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/#content'); ?>">Content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://info-merintisindonesia.medium.com/">Blog</a>
                </li>
                <li class="nav-item">
                    <!-- Login/Logout -->
                    <div id="link-log">
                        <?= ($isLogin ? '<a class="nav-link" href="javascript:void(0)" onclick="logout()">Sign Out</a>' : '<a class="nav-link" id="btnSignIn" href="'.base_url('/signin').'">Sign In</a>'); ?>
                    </div>
                    <!-- End Login/Logout -->
                </li>
            </ul>
        </div>
    </nav>
</header>
<?= $this->endSection(); ?>

<?= $this->section('content'); ?>
<section id="program" class="mt-48">
    <div class="container">
        <div class="menu-program">
            <ul class="text-center">
                <li class="menu-active roboto-condensed let-space-08" id="past-program">
                    <a href="javascript:void(0)" class="link-none">PAST PROGRAM</a>
                </li>
                <li class="roboto-condensed let-space-08" id="ongoing-program">
                    <a href="javascript:void(0)" class="link-none">ON-GOING PROGRAM</a>
                </li>
                <li class="roboto-condensed let-space-08" id="upcoming-program">
                    <a href="javascript:void(0)" class="link-none">UPCOMING PROGRAM</a>
                </li>
            </ul>
        </div>
        <!-- DATA PROGRAM -->
        <div id="data-program" class="mt-32">
            <!-- ON LOADING -->
            <div class="my-4 d-flex justify-content-center" id="loader">
                <strong class="text-center">Loading...</strong>
                <div class="mx-4 loader"></div>
            </div>

            <div class="d-flex flex-wrap" id="loc-data">

            </div>
        </div>
        <!-- END DATA PROGRAM -->
    </div>    
    
</section>
<?= $this->endSection(); ?>

<?= $this->section('addScript'); ?>
<script>
// Tambahan Script

  // SMOOTH SCROLL
  $(document).ready(function() {
    $("a").on("click", function(event) {
      if (this.hash == "#program") {
        event.preventDefault();
        let hash = this.hash;

        $("html, body").animate({
          scrollTop: $(hash).offset().top
        }, 800, function() {
          window.location.hash = hash;
        });
      }
    });
  });

  // SHOW NAVBAR FIXED
  window.onscroll = changeNav;

  function changeNav() {
    let navbar = $('nav');
    if (window.pageYOffset > 90) {
      navbar.addClass('navbar-fixed')
    } else {
      navbar.removeClass('navbar-fixed');
    }
  }

  // PROSES MENGOLAH DATA
  function getPastProg() {
    // Ambil Data PAST PROGRAM
    $.ajax({
        type: "get",
        url: "<?= base_url('/home/pastprogram'); ?>",
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
            },
        success: function(data) {
            // console.log(data)
            let dtPast = ''
            $.each(data, function(index, val) {
                dtPast += 
                `
                <div class="card border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="${val.link_dokumentasi}" target="_blank">
                            <img src="<?= base_url('/assets/img/program'); ?>/${val.link_pamflet}" alt="${val.nama_program}" class="img-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })
            $('#loc-data').html(dtPast)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }

  // PANGGIL PAST PROGRAM
  getPastProg()

  // MOVE TAB PROGRAM
  $(document).ready(function() {
      // PAST PROGRAM  
      $('#past-program').on("click", function() {
        $('#past-program').addClass('menu-active')
        $('#ongoing-program').removeClass('menu-active')
        $('#upcoming-program').removeClass('menu-active')
        // AMBIL PAST PROGRAM
        getPastProg()
      })
      // ONGOING PROGRAM
      $('#ongoing-program').on("click", function() {
        $('#past-program').removeClass('menu-active')
        $('#ongoing-program').addClass('menu-active')
        $('#upcoming-program').removeClass('menu-active')
        // AMBIL ONGOING PROGRAM
        $.ajax({
            type:"get",
            url:"<?= base_url('/home/ongoingprogram') ?>",
            beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
            },
            success: function(data) {
                console.log(data)
                let dtOngo = ''
                $.each(data, function(index, val) {
                    let endDate = new Date(val.tgl_selesai)
                    let startDate = new Date(val.tgl_mulai)
                    startDate = startDate.toDateString()
                    endDate = (endDate.toDateString() == "Invalid Date") ? "" : "- "+endDate.toDateString()

                    dtOngo += 
                    `
                    <div class="card border-none m-sm-2 bg-white rounded">
                        <div class="card-body px-1 px-sm-2 py-2 border-gold">
                        <img src="<?= base_url('/assets/img/program'); ?>/${val.link_pamflet}" alt="${val.nama_program}" class="img-180 mx-1 x-md-4 my-2">
                            <p class="roboto" style="width: 150px;">
                            ${val.nama_program}: <br>
                            <span class="roboto-condensed">${val.nama_kegiatan}</span>
                            </p>
                            <table style="width: 150px; font-size: 14px;">
                                <tr>
                                    <td><small>${startDate}</small></td>
                                    <td><small>${val.jam_mulai}</small></td>
                                </tr>
                                <tr>
                                    <td><small>${endDate}</small></td>
                                    <td><small>- ${val.jam_selesai} WIB</small></td>
                                </tr>
                            </table>
                            <p class="roboto-condensed" style="width: 150px; font-size: 12px;">*${val.sasaran_program}</p>
                        </div>
                        <a href="<?= base_url('/program/detail/ongoing') ?>/${val.id}" class="btn-kuning link-none text-white d-block mx-auto my-2 text-center">DETAIL</a>
                    </div>
                    `
                })
                $('#loc-data').html(dtOngo)
                $("#loader").removeClass('d-flex');
                $("#loader").addClass('d-none');
            },
            error: function(err) {
                console.error(err)
            }
        })
      })
      // UPCOMING PROGRAM
      $('#upcoming-program').on("click", function() {
        $('#past-program').removeClass('menu-active')
        $('#ongoing-program').removeClass('menu-active')
        $('#upcoming-program').addClass('menu-active') 
        // AMBIL UPCOMING PROGRAM
        let dtUpco = 
        `<div id="data-program" class="d-block mx-auto">
            <p class="text-center mt-32">Stay tuned on instagram <a href="https://www.instagram.com/merintis.indonesia/?hl=en" target="_blank"><b>@merintis.indonesia</b></a></p>
        </div>`
        $('#loc-data').html(dtUpco)
      })
  })

  // ==================== LOGOUT ===================
  function logout() {
    //console.log("Berhasil logout");
    // --------------- LOGOUT -------------------
    $(this).on("click", function() {
      // ------ MENGHAPUS SESSION DI DATABASE
      $('#link-log').addClass("active");
      let session_id = "<?= $idAkun ?>";
      let session_token = "<?= $token ?>";
      $.ajax({
        type: "post",
        url: "<?= base_url('/akun/hapussession'); ?>",
        data: {
          "id_akun": session_id
        },
        success: function() {
          window.location.replace("<?= base_url(); ?>");
        }
      })
      // ------ END MENGHAPUS SESSION DI DATABASE
    })
  }
  // ==================== END LOGOUT ===============

  // ========== BTN SIGNIN
  $('#btnSignIn').on("click", function() {
    $('#link-log').addClass("active");
  })
  // ========== END BTN SIGNIN
</script>
<?= $this->endSection(); ?>