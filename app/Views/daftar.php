<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Merintis Indonesia Summit 2021</title>
  <!-- SEO -->
  <meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
  <meta name="keywords" content="Startup,UMKM,UKM,Daerah,Produksi,Merintis Indonesia,merintisindonesia,Madiun,Jawa Timur,Ide Bisnis,Technopreneur,Wirausaha,Startup Madiun,Digital,business ideas">
  <meta name="og:locale" content="id_ID">
  <meta name="og:type" content="website">
  <meta name="og:title" content="Merintis Indonesia">
  <meta name="og:description" content="Merintis Indonesia - Connect, Collaborate & Create">
  <meta name="og:url" content="https://merintisindonesia.com">
  <meta name="og:site_name" content="Merintis Indonesia">
  <meta name="og:image" content="https://merintisindonesia.com/assets/img/assets/Picture15.png">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="Merintis Indonesia">
  <meta name="twitter:description" content="Yuk Jadi Pengusaha Muda Bersama Merintis Indonesia!">
  <!-- END SEO -->

  <!-- Favicons -->
	<link href="<?= base_url('assets/img/assets/Picture17.png'); ?>" type="image/png" rel="icon">
  <link href="<?= base_url('assets/img/assets/Picture17.png'); ?>" rel="apple-touch-icon">

  <link rel="stylesheet" href="<?= base_url('assets/vendors/bootstrap/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/fontawesome/css/all.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/nice-select/nice-select.css'); ?>">
  <script src="https://use.fontawesome.com/60a313a36b.js"></script>

  <link rel="stylesheet" href="<?= base_url('assets/css/style.css');?>">
  <style>
    .kuning {
      color: #f9aa32;
    }
    .red {
      color: red;
    }
    .hijau {
      color: #45b54b;
    }
    .size-100px {
      font-size: 100px;
    }
    .logo {
      max-width: 100px;
    }
    .bg-kuning {
      background-color: #ffd9a1;
    }
    .sponsor img {
      max-height: 50px;
      border-radius: 7px;
    }
    .fluid {
      max-width: 165px;
      max-height: 165px;
    }
    .img-small {
      max-width: 100px;
      padding-right: 20px;
    }
    .pd-top {
      padding-top: 20px;
    }
    .padding-top-small {
      padding-top: 15px;
    }
    .has-error input, select{
      border-width: 1px;
      border-color: red;
      border-style: solid;
    }
    @media (min-height:1000px) {
      .padding-top-content {
        padding-top: 100px;
      }
    }
    @media (max-height:999px) {
      .padding-top-content {
        padding-top: 80px;
      }
    }
    table.table-custom {
      /* table-layout: fixed; */
      width: 100%;
    }
    table.table-custom td {
      padding: 10px;
      /* word-wrap: break-word; */
    }
    table.table-contact {
      table-layout: fixed;
      width: 100%;
    }
    table.table-contact td {
      padding: 10px;
      word-wrap: break-word;
    }
    .text-center {
      text-align: center;
    }
  </style>
</head>
<body class="bg-light">
<!--============================ NAVBAR -->
<nav class="navbar navbar-light bg-light px-sm-5 shadow-sm mb-5">
  <div class="d-inline-block">
    <a class="navbar-brand" href="<?= base_url(); ?>">
      <img src="<?= base_url('assets/img/assets/Picture1.png'); ?>" class="logo" alt="logo-mis">
      <span class="heading hijau">Merintis</span> <span class="heading kuning">Indonesia</span>
    </a>
  </div>
</nav>
<!--============================ END NAVBAR -->

<div class="container">
  <div class="row justify-content-center mb-5">
    <div class="col-md-7 col-lg-5">
      <!--============================ CARD DAFTAR -->
      <div class="card text-center">
        <div class="card-body px-5">
          <h5 class="card-title">Daftar Akun Baru</h5>
          <!--================ ALERT -->
          <div id="alert-info"></div>
          <!--================ END ALERT -->
          <form action="#" method="post" id="formDaftar" class="mt-3 custom-form">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama">
                  <div class="invalid-feedback">
                    Pesan error nama
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Email" name="email">
                  <div class="invalid-feedback">
                    Pesan error email
                  </div>
                </div>
                <div class="form-group">
                  <div class="lay-password-outline">
                    <input type="password" class="form-control password" placeholder="Password" name="password">
                    <a href="javascript:void(0)">
                      <i class="fa fa-eye-slash"></i>
                    </a>
                  </div>
                  <div id="err-passwd" class="invalid-feedback">
                    Pesan error password
                  </div>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Konfirmasi Password" name="pass_confirm">
                    <div class="invalid-feedback">
                      Pesan error konfirmasi password
                    </div>
                </div>
              </div>
            </div>
          </form>
          <button id="btnDaftarAkun" class="button button-hero">Daftar</button>
        </div>
        <!--============================ END CARD DAFTAR -->
      </div>
    </div>
  </div>
</div>

<script src="<?= base_url('assets/vendors/jquery/jquery-3.2.1.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendors/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15/dist/smooth-scroll.polyfills.min.js"></script>
<script type="text/javascript">
//------------ KIRIM DATA ---------------
$('#btnDaftarAkun').on('click', function() {
  $.ajax({
    type: "post",
    url: "<?= base_url('/akun/buatakun'); ?>",
    data: $('#formDaftar').serialize(),
    beforeSend: function() {
      //------DISABLED BUTTON
      $("#btnDaftarAkun").attr('disabled', true);
      $("#btnDaftarAkun").addClass('wait');
    },
    success: function(data) {
      if (data.success) {
          // console.log(data.message);
          alertBerhasil(data.message);
          // ---- MENGHAPUS SEMUA INFO ERROR
          $(".form-control").eq(0).removeClass("is-invalid");
          $(".form-control").eq(1).removeClass("is-invalid");
          $(".lay-password-outline").removeClass("password-invalid");
          $(".form-control").eq(3).removeClass("is-invalid");
          //------ENABLED BUTTON
          $("#btnDaftarAkun").attr('disabled', false);
          $("#btnDaftarAkun").removeClass('wait');
          // ----------- FUNCTION PASSWORD SHOW INVALID MSG -----------------
          passwdErr();
          // ----------- END FUNCTION PASSWORD SHOW INVALID MSG -----------------
          // ----------- REDIRECT KE LOGIN -----------------
          window.location.replace("<?= base_url(); ?>/?stat=1");
          // ----------- END REDIRECT LOGIN -----------------
      } else {
          // console.log(data.error);
          //----RESET ALERT
          $('#alert-info').html('');
          //------ENABLED BUTTON
          $("#btnDaftarAkun").attr('disabled', false);
          $("#btnDaftarAkun").removeClass('wait');
          // ----------- ERROR NAMA LENGKAP
          if (data.error.nm_lengkap) {
            $(".form-control").eq(0).addClass("is-invalid");
            $(".invalid-feedback").eq(0).html(data.error.nm_lengkap);
          } else {
            $(".form-control").eq(0).removeClass("is-invalid");
          }
          // ----------- ERROR EMAIL
          if (data.error.email) {
            $(".form-control").eq(1).addClass("is-invalid");
            $(".invalid-feedback").eq(1).html(data.error.email);
          } else {
            $(".form-control").eq(1).removeClass("is-invalid");
          }
          // ----------- ERROR PASSWORD
          if (data.error.password) {
            $(".lay-password-outline").addClass("password-invalid");
            $(".invalid-feedback").eq(2).html(data.error.password);
          } else {
            $(".lay-password-outline").removeClass("password-invalid");
          }
          // ----------- ERROR KONFIRMASI PASSWORD
          if (data.error.pass_confirm) {
            $(".form-control").eq(3).addClass("is-invalid");
            $(".invalid-feedback").eq(3).html(data.error.pass_confirm);
          } else {
            $(".form-control").eq(3).removeClass("is-invalid");
          }
          // ----------- END ERROR KONFIRMASI PASSWORD

          // ----------- FUNCTION PASSWORD SHOW INVALID MSG -----------------
          passwdErr();
          // ----------- END FUNCTION PASSWORD SHOW INVALID MSG -----------------
      }
    },
    error: function(response) {
      // database offline tulis error disini
      // console.log(response.responseJSON.message);
      alertGagal(response.responseJSON.message);
      //------ENABLED BUTTON
      $("#btnDaftarAkun").attr('disabled', false);
      $("#btnDaftarAkun").removeClass('wait');
    }
  })
})
//------------ END KIRIM DATA ---------------

//------------ SHOW PASSWORD ---------------
$(document).ready(function() {
  $(".lay-password-outline a").on('click', function(event) {
    event.preventDefault();
    if($('.lay-password-outline input').attr("type") == "text"){
      $('.lay-password-outline input').attr('type', 'password');
      $('.lay-password-outline i').addClass( "fa-eye-slash" );
      $('.lay-password-outline i').removeClass( "fa-eye" );
    }else if($('.lay-password-outline input').attr("type") == "password"){
      $('.lay-password-outline input').attr('type', 'text');
      $('.lay-password-outline i').removeClass( "fa-eye-slash" );
      $('.lay-password-outline i').addClass( "fa-eye" );
    }
  });
});
//------------ END-SHOW PASSWORD ---------------

// ----------- FUNCTION PASSWORD SHOW INVALID MSG -----------------
function passwdErr() {
  if($(".lay-password-outline").hasClass("password-invalid"))
  {
    $("#err-passwd").show();
  } else {
    $("#err-passwd").hide();
  }
}
// ----------- END FUNCTION PASSWORD SHOW INVALID MSG -----------------

//------------ ALERT FUNCTION ---------------
function alertBerhasil(pesan) {
  let alBerhasil = `
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  `
  $('#alert-info').html(alBerhasil)
}

function alertGagal(pesan) {
  let alGagal = `
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div id="text-berhasil">${pesan}</div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  `
  $('#alert-info').html(alGagal)
}
//------------ END ALERT FUNCTION ---------------

</script>
</body>
</html>
