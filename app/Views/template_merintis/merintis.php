<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?= $this->renderSection('head-title'); ?>
  <!-- SEO -->
  <?= $this->renderSection('meta-description'); ?>
  <meta name="keywords" content="Startup,UMKM,UKM,Daerah,Produksi,Merintis Indonesia,merintisindonesia,Madiun,Jawa Timur,Ide Bisnis,Technopreneur,Wirausaha,Startup Madiun,Digital,business ideas">
  <meta name="og:locale" content="id_ID">
  <meta name="og:type" content="website">
  <meta name="og:title" content="Merintis Indonesia">
  <meta name="og:description" content="Merintis Indonesia - Connect, Collaborate & Create">
  <meta name="og:url" content="https://merintisindonesia.com">
  <meta name="og:site_name" content="Merintis Indonesia">
  <meta name="og:image" content="https://merintisindonesia.com/assets/img/Picture15.png">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="Merintis Indonesia">
  <meta name="twitter:description" content="Yuk Jadi Pengusaha Muda Bersama Merintis Indonesia!">
  <!-- END SEO -->

  <!-- Favicons -->
  <link href="<?= base_url('favicon.png'); ?>" type="image/png" rel="icon">
  <link href="<?= base_url('apple-touch-icon.png'); ?>" rel="apple-touch-icon">

  <link rel="stylesheet" href= "<?= base_url('assets/vendors/bootstrap/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/vendors/fontawesome/css/all.min.css'); ?>">
  <script src="https://use.fontawesome.com/60a313a36b.js"></script>

  <?= $this->renderSection('addCSS'); ?>
</head>
<body>
  <!--================ Header Menu Area start =================-->
  <?= $this->renderSection('header'); ?>  
  <!--================ END Header Menu Area =================-->

  <?= $this->renderSection('content'); ?>

  <?= $this->renderSection('footer'); ?>

  <script src="<?= base_url ('assets/vendors/jquery/jquery-3.2.1.min.js'); ?>"></script>
  <script src="<?= base_url ('assets/vendors/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
  <?= $this->renderSection('addScript'); ?>
</body>
</html>
