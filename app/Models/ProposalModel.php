<?php namespace App\Models;

use CodeIgniter\Model;

class ProposalModel extends Model
{
    protected $table = 'data_proposal';
    protected $primarykey = 'id';
    protected $allowedFields = ['id_user', 'no_hp', 'tempat_lahir', 'tgl_lahir', 'kota_domisili', 'pekerjaan', 'jenis_kelamin', 'role', 'url_proposal', 'bidang_bisnis', 'metode_bayar', 'url_bukti_bayar'];

    public function dataProposal() {
      //SELECT * FROM `user_akun` WHERE email="coba@coba.com";
      $builder = $this->db->query("SELECT ak.nm_lengkap, ak.email, dp.no_hp, dp.tempat_lahir, dp.tgl_lahir, dp.kota_domisili, dp.pekerjaan, dp.jenis_kelamin, dp.role, dp.url_proposal, dp.bidang_bisnis, dp.metode_bayar, dp.url_bukti_bayar FROM data_proposal as dp RIGHT JOIN user_akun as ak ON ak.id = dp.id_user WHERE ak.id != 69 AND ak.id != 21");
      return $builder;
    }
}
