<?php namespace App\Models;

use CodeIgniter\Model;

class PengumumanModel extends Model
{
  protected $table = 'tbl_finalis50';
  protected $primarykey = 'id_finalis';
  protected $allowedFields = ['id_akun', 'nama_bisnis', 'link_yt'];

  public function tampilData() {
    $builder = $this->db->query("SELECT fn.id_finalis, ak.nm_lengkap, fn.nama_bisnis FROM tbl_finalis50 as fn INNER JOIN user_akun as ak ON fn.id_akun = ak.id");
    return $builder;
  }

  public function cariData($nama_tim) {
    $builder = $this->db->query("SELECT fn.id_finalis, ak.nm_lengkap, fn.nama_bisnis FROM tbl_finalis50 as fn INNER JOIN user_akun as ak ON fn.id_akun = ak.id WHERE ak.nm_lengkap LIKE '%".$nama_tim."%'");
    return $builder;
  }

  public function simpanLink($id_akun, $linkvideo) {
    $builder = $this->db->query("UPDATE tbl_finalis50 SET link_yt = '".$linkvideo."' WHERE id_akun=".$id_akun);
    return $builder;
  }

  public function cekFinalis($id_akun) {
    $builder = $this->db->query("SELECT * FROM tbl_finalis50 WHERE id_akun=".$id_akun);
    return $builder;
  }

  public function tampilAll() {
    $builder = $this->db->query("SELECT fn.id_finalis, ak.nm_lengkap, fn.nama_bisnis, fn.link_yt FROM tbl_finalis50 as fn INNER JOIN user_akun as ak ON fn.id_akun = ak.id");
    return $builder;
  }
}
