<?php namespace App\Models;

use CodeIgniter\Model;

class ProgramModel extends Model
{
  protected $table = 'data_program';
  protected $primarykey = 'id';
  protected $allowedFields = [
      'nama_program', 
      'nama_kegiatan',
      'link_pamflet',
      'link_daftar',
      'desc_program',
      'sasaran_program',
      'tgl_mulai',
      'tgl_selesai',
      'jam_mulai',
      'jam_selesai',
      'biaya',
      'harga_normal',
      'harga_promo',
      'link_jadwal',
      'link_dokumentasi',
      'status'
    ];

  public function dataProgram() {
    //SELECT * FROM `user_akun` WHERE email="coba@coba.com";
    $builder = $this->db->query("SELECT id, nama_program, nama_kegiatan, link_pamflet, link_daftar, desc_program, sasaran_program, tgl_mulai, tgl_selesai, DATE_FORMAT(jam_mulai, '%H:%i') as jam_mulai, DATE_FORMAT(jam_selesai, '%H:%i') as jam_selesai, biaya, harga_normal, harga_promo, link_jadwal, link_dokumentasi, status FROM data_program");
    return $builder;
  }

  public function getByIdProgram($id) {
    $builder = $this->db->query("SELECT id, nama_program, nama_kegiatan, link_pamflet, link_daftar, desc_program, sasaran_program, tgl_mulai, tgl_selesai, DATE_FORMAT(jam_mulai, '%H:%i') as jam_mulai, DATE_FORMAT(jam_selesai, '%H:%i') as jam_selesai, biaya, harga_normal, harga_promo, link_jadwal, link_dokumentasi, status FROM data_program WHERE id=$id");
    return $builder;
  }

  // Past
  public function getPastProgram() {
    $builder = $this->db->query("SELECT nama_program, link_pamflet, link_dokumentasi FROM data_program WHERE status='past'");
    return $builder;
  }

  // On-Going
  public function getOngoingProgram() {
    $builder = $this->db->query("SELECT id, nama_program, nama_kegiatan, link_pamflet, link_daftar, desc_program, sasaran_program, tgl_mulai, tgl_selesai, DATE_FORMAT(jam_mulai, '%H:%i') as jam_mulai, DATE_FORMAT(jam_selesai, '%H:%i') as jam_selesai FROM data_program WHERE status='on-going'");
    return $builder;
  }
  
  // By Id ON-Going
  public function getByIdOngoing($id) {
    $builder = $this->db->query("SELECT nama_program, nama_kegiatan, link_pamflet, link_daftar, desc_program, sasaran_program, tgl_mulai, tgl_selesai, DATE_FORMAT(jam_mulai, '%H:%i') as jam_mulai, DATE_FORMAT(jam_selesai, '%H:%i') as jam_selesai, link_jadwal, link_dokumentasi, status FROM data_program WHERE id=$id");
    return $builder;
  }

  // Upcoming
  public function getUpcomingProgram() {
    $builder = $this->db->query("SELECT id, nama_program, nama_kegiatan, link_pamflet, link_daftar, desc_program, sasaran_program, tgl_mulai, tgl_selesai, DATE_FORMAT(jam_mulai, '%H:%i') as jam_mulai, DATE_FORMAT(jam_selesai, '%H:%i') as jam_selesai, biaya, harga_normal, harga_promo, link_jadwal, link_dokumentasi, status FROM data_program WHERE status='upcoming'");
    return $builder;
  }

}
